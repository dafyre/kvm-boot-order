# kvm-boot-order

An easy way to manage VM Boot order for the KVM hypervisor for Ubuntu / Debian based systems.

All commands performed below should be done as root or with sudo.

**NOTE:** In order for this to work properly, your VMs need to have the Automatically Start at boot option turned off.


# Automatic Service Setup

To set up the service, run  ```./bootOrder-setup```


# Set KVM_BOOT_ORDER and Wait time

Edit the /etc/default/kvm-boot-order file and set the VMs that you want to start in the variable KVM\_BOOT\_ORDER like below (the VM Names should match the output given by ```virsh list --name --all```):

KVM\_BOOT\_ORDER="Server2012 deb-dhcp centos7-apache freepbx14"

Set the KVM\_BOOT\_WAIT value (in seconds).  The default is 30 seconds.  The system will wait this number of seconds before starting the next VM.

If No value is set for KVM\_BOOT\_ORDER, then it will not start any VMS!

# Manual Setup Instructions

**Copy kvm-boot-order.default to /etc/default/kvm-boot-order**

```cp ./kvm-boot-order.default /etc/default/kvm-boot-order```

**Copy kvm-boot-order.service /etc/systemd/system**

```cp ./kvm-boot-order.service /etc/systemd/system/```

**Copy bootOrder to /usr/local/bin/**

```cp ./bootOrder /usr/local/bin/```

**Enable kvm-boot-order service**

```systemctl daemon-reload```
```systemctl enable kvm-boot-order```
